//============================
//2D Schrodinger Equation
//XENOS Konstantinos Odysseas
//============================
#include <complex>
#include <boost/multi_array.hpp>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

using namespace boost;
using namespace std::complex_literals;

using complex = std::complex<double>;
using carray2d = boost::multi_array<std::complex<double>, 2>;
using array1d = boost::multi_array<double, 1>;

//================
//Write function
//================
void write(std::ofstream& ofs,
           double t,
           const array1d& x,
           const array1d& y,
           const carray2d& u,
           size_t nx,
           size_t ny){
  ofs << "#time = " << t << std::endl;
  for (size_t ix = 0; ix < nx; ++ix) {
    for (size_t iy = 0; iy < ny; ++iy) {
      ofs << x[ix] << " " <<y[iy] << " " <<std::norm(u[ix][iy])<< std::endl; //u is complex, we write the norm
    }
    ofs << std::endl;
  }
  ofs << std::endl;
}

//===================
//Boundary conditions
//===================
//The particle is trapped a box of 1x1 dimensions at all times, which means:
//u(0,y,t) = u(1,y,t) = u(x,0,t) = u(x,1,t) = 0
void boundaryConditions(carray2d& u, size_t nx, size_t ny) {
  for (size_t iy = 0; iy < ny; ++iy) {
    u[0][iy]=0;
    u[nx - 1][iy] = 0.0;
  }
  for (size_t ix = 0; ix < nx; ++ix) {
    u[ix][0] = 0.0;
    u[ix][ny - 1] = 0.0;
  }
 }

int main(){

  //=============
  //Spatial Grid
  //=============
  double xmin = 0.0;
  double xmax = 1.0;
  double ymin = 0.0;
  double ymax = 1.0;

  size_t nx = 100;
  size_t ny = 100;

  array1d x(extents[nx]);
  array1d y(extents[ny]);

  double dx = (xmax - xmin) / (nx - 1.0);
  double dy = (ymax - ymin) / (ny - 1.0);

  for(size_t ix = 0; ix < nx; ++ix){
    x[ix] = xmin + ix * dx;
  }

  for(size_t iy = 0; iy < ny; ++iy){
    y[iy] = ymin + iy * dy;
  }

  //==========================================
  //Here we define the arrays that we will use
  //==========================================
  carray2d u(extents[nx][ny]);
  carray2d uk(extents[nx][ny]);
  carray2d uprev(extents[nx][ny]);
  carray2d rhs(extents[nx][ny]);

  //=======================================
  //Initial Condition: Gaussian wave packet
  //=======================================
  double xc = (xmax + xmin) * 0.5;
  double yc = (ymax + ymin) * 0.5;
  double sigmax = (xmax - xmin) * 0.1;
  double sigmay = (ymax - ymin) * 0.1;
  double sum = 0.0;
  double efactorx = 0.5 / (sigmax * sigmax);
  double efactory = 0.5 / (sigmay * sigmay);

  for (size_t ix = 1; ix < nx; ++ix) {
    double deltax2 = (x[ix] - xc) * (x[ix] - xc);
    double argx = efactorx * deltax2;
    for (size_t iy = 1; iy < ny; ++iy) {
      double deltay2 = (y[iy] - yc) * (y[iy] - yc);
      double argy = efactory * deltay2;

      u[ix][iy] = exp(-( argx + argy )); //Gaussian normalized to 1
      double norm = std::norm(u[ix][iy]);
      sum += norm;
    }
  }

  boundaryConditions(u,nx,ny);

  //========================
  //Write initial conditions
  //========================
  std::ofstream ofs("2DSchrodinger.txt");
  ofs << "#title = 2D Schrodinger equation" << std::endl;
  ofs << "#dim   = 2" << std::endl;
  ofs << "#xmin  = " << xmin << std::endl;
  ofs << "#xmax  = " << xmax << std::endl;
  ofs << "#ymin  = " << ymin << std::endl;
  ofs << "#ymax  = " << ymax << std::endl;
  ofs << "#zmin  = " << -1.1 << std::endl;
  ofs << "#zmax  = " << +1.1 << std::endl;

  write(ofs, 0.0, x, y, u, nx, ny);

  //====================================================
  //Numerical solution using the Crank - Nicolson scheme
  //====================================================
  double dx2 = dx * dx;
  double dy2 = dy * dy;
  double dt = dx2 * dy2 / ( dx2 + dy2 );
  double alphax = dt / dx2;
  double alphay = dt / dy2;

  complex cx(0.0, alphax * 0.5); // \frac{I*Δt}{2*Δx^2}
  complex cy(0.0, alphay * 0.5); // \frac{I*Δt}{2*Δy^2}
  //The factors below appear when solving for u^{n+1}_{i,j}
  complex Cx = cx / (1. + 2. * (cx + cy)); //Concerns u^{n+1}_{ix +- 1}{iy}
  complex Cy = cy / (1. + 2. * (cx + cy)); //Concerns u^{n+1}_{ix}{iy +- 1}
  complex Ci = 1. / (1. + 2. * (cx + cy)); //Concerns the "right hand side" terms

  double t = 0.0;
  size_t nt = 5000;
  size_t snapshot = 10;

  //==============================================
  //We calculate the right hand side of the system
  //==============================================
  for (size_t it = 1; it < nt; ++it) {
    uk = u;
    t = it * dt;
    for (size_t ix = 1; ix < nx - 1; ix++){
      for (size_t iy = 1; iy < ny - 1; iy++){
        rhs[ix][iy] = cx * ( uk[ix + 1][iy] - 2.0 * uk[ix][iy] + uk[ix - 1][iy] )
                    + cy * ( uk[ix][iy + 1] - 2.0 * uk[ix][iy] + uk[ix][iy - 1] )
                    + uk[ix][iy];
      }
    }

  double rtol = 1.e-6; //Our required percision

    for(;;){
      uprev = u; //store and copy the value of previous iterate
      double rdiff = 0.0;
      for (size_t ix = 1; ix < nx - 1; ix++){
        for (size_t iy = 1; iy < ny - 1; iy++){
            u[ix][iy] = Cx * ( u[ix + 1][iy] + u[ix - 1][iy] )
                      + Cy * ( u[ix][iy + 1] + u[ix][iy - 1] )
                      + Ci * rhs[ix][iy];
             rdiff = std::max(rdiff, abs(u[ix][iy]/uprev[ix][iy] - 1.0));
        }
      }
      if (rdiff <= rtol){
        break; //leave for loop when requiremet is met
      }
      boundaryConditions(u,nx,ny);
    }
    if (!(it % snapshot)) {
      write(ofs,t,x,y,u,nx,ny);
    }
  }
  return 0;
}
